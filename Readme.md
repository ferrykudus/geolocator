### Installation virtualenv
running with python 3.8 
```sh
virtualenv /usr/bin/python3.8 -p <folder name>
source <folder name>/bin/activate
```

### choose mode to run flask env
```sh
export FLASK_ENV=<development,production,stagging>
```
choose one env:
1. development
2. production
3. stagging

change config at app/config.py with your credentials

### Installation env
```sh
pip install -r requirements.txt
```

```
run with python run.py at folder app
```