import os
from flask import request, json, Response, Blueprint, g, url_for, session, render_template, request, url_for, redirect, flash
from geopy.geocoders import Nominatim
from geopy import distance
from werkzeug.utils import secure_filename
from datetime import datetime
from config import GetConfig as cfg

services_distance = Blueprint('services_distance', __name__)
cfg.set('location','moscow ring road')

@services_distance.route('/', methods=['GET'], strict_slashes=False)
def index_services():
    return custom_response({'data':None, 'message':'module services distance running', 'status':200 },200)

# Logic distance
@services_distance.route('/location', methods=['POST','GET'])
def detail_location():
    if request.method == 'POST':
        req_data = request.json
        geolocator = Nominatim(user_agent="app_locator_workana")
        location_center = geolocator.geocode("{location_format}".format(location_format=cfg.config('location')),language="en")
        location_center_raw = location_center.raw

        if 'lat' not in req_data:
            req_data['lat'] = 0

        if 'long' not in req_data:
            req_data['long'] = 0

        location_request = geolocator.reverse("{lat}, {long}".format(long=req_data['long'], lat=req_data['lat']),language="en")
        location_request_raw = location_request.raw
        if req_data['location'] not in location_request.address:
            return custom_response({'data': None, 'message': 'location given is mistake the true is {message} with Longitude {lat}, Latitude {long} '
                                   .format(message=location_request.address,long=location_request.longitude, lat=location_request.latitude), 'status': 501}, 501)

        if req_data['location'] is None or req_data['location'] == '':
            return custom_response({'data': None, 'message': 'location cannot be empty', 'status': 501}, 501)

        moscow_ring_road = (location_center_raw['lat'],location_center_raw['lon'])
        location_now = (location_request_raw['lat'],location_request_raw['lon'])
        distance_now = distance.distance(moscow_ring_road, location_now).km
        return custom_response({'data':{
            'distance':'{distance} km'.format(distance=distance_now),
            'location_now':'{location}'.format(location=location_now),
            'center_point':'{center}'.format(center=moscow_ring_road)
        },
            'message':'data distance calculated',
            'status':200},200)
    else:
        return custom_response({'data':None,'message':'please post the parameters', 'status':422},422)


def custom_response(res, status_code):
    return Response(
        mimetype="application/json",
        response=json.dumps(res),
        status=status_code)