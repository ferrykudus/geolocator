import os
import tempfile

import pytest

from app import create_app

@pytest.fixture
def client():
    db_fd, db_path = tempfile.mkstemp()
    app = create_app({'TESTING': True, 'DATABASE': db_path})

    with app.test_client() as client:
        with app.app_context():
            init_db()
        yield client

    os.close(db_fd)
    os.unlink(db_path)

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 1025))
    env_name = os.getenv('FLASK_ENV')
    app = create_app(env_name)
    #run app
    app.run( host='0.0.0.0', port=port, threaded=True)
