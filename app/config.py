import os

base_dir = os.path.abspath(os.path.dirname(__file__))

# Production Setting
class GetConfig:
    __conf = {
        "username": None,
        "password": None,
        "dsn":None,
        "database":None,
        "connection": None,
        "port": None,
        "location": None,
        "secret_key": os.urandom(16).hex(),
        "upload_folder": "static",
        "file_path": None,
        "file":None,
        "MAIL_SERVER":None,
        "MAIL_PORT":None,
        "MAIL_USE_TLS":False,
        "MAIL_USE_SSL":False,
        "MAIL_DEBUG":False,
        "MAIL_USERNAME":None,
        "MAIL_PASSWORD":None,
        "MAIL_DEFAULT_SENDER":None,
        "MAIL_MAX_EMAILS":None,
        "MAIL_SUPPRESS_SEND":None,
        "MAIL_ASCII_ATTACHMENTS":False
    }
    __setters = [
                 "username",
                 "password",
                 "dsn",
                 "database",
                 "connection",
                 "secret_key",
                 "port",
                 "location",
                 "upload_folder",
                 "file",
                 "file_path",
                 "MAIL_SERVER",
                 "MAIL_PORT",
                 "MAIL_USE_TLS",
                 "MAIL_USE_SSL",
                 "MAIL_DEBUG",
                 "MAIL_USERNAME",
                 "MAIL_PASSWORD",
                 "MAIL_DEFAULT_SENDER",
                 "MAIL_MAX_EMAILS",
                 "MAIL_SUPPRESS_SEND",
                 "MAIL_ASCII_ATTACHMENTS"
               ]

    def __init__(self):
        pass

    @staticmethod
    def config(name):
        return GetConfig.__conf[name]

    @staticmethod
    def set(name, value):
        if name in GetConfig.__setters:
            GetConfig.__conf[name] = value
        else:
            raise NameError("Name not accepted in set() method")

# Development Setting
class Development:
    __conf = {
        "username": None,
        "password": None,
        "dsn":None,
        "database":None,
        "connection": None,
        "port": None,
        "location": None,
        "secret_key": os.urandom(16).hex(),
        "upload_folder": "static",
        "file_path": None,
        "file": None,
        "MAIL_SERVER": None,
        "MAIL_PORT": None,
        "MAIL_USE_TLS": False,
        "MAIL_USE_SSL": False,
        "MAIL_DEBUG": False,
        "MAIL_USERNAME": None,
        "MAIL_PASSWORD": None,
        "MAIL_DEFAULT_SENDER": None,
        "MAIL_MAX_EMAILS": None,
        "MAIL_SUPPRESS_SEND": None,
        "MAIL_ASCII_ATTACHMENTS": False
    }
    __setters = [
        "username",
        "password",
        "dsn",
        "database",
        "connection",
        "secret_key",
        "port",
        "location",
        "upload_folder",
        "file",
        "file_path",
        "MAIL_SERVER",
        "MAIL_PORT",
        "MAIL_USE_TLS",
        "MAIL_USE_SSL",
        "MAIL_DEBUG",
        "MAIL_USERNAME",
        "MAIL_PASSWORD",
        "MAIL_DEFAULT_SENDER",
        "MAIL_MAX_EMAILS",
        "MAIL_SUPPRESS_SEND",
        "MAIL_ASCII_ATTACHMENTS"
    ]

    def __init__(self):
        pass

    @staticmethod
    def config(name):
        return Development.__conf[name]

    @staticmethod
    def set(name, value):
        if name in Development.__setters:
            Development.__conf[name] = value
        else:
            raise NameError("Name not accepted in set() method")


# Production Stagging
class Stagging:
    __conf = {
        "username": None,
        "password": None,
        "dsn":None,
        "database":None,
        "connection": None,
        "port": None,
        "location": None,
        "secret_key": os.urandom(16).hex(),
        "upload_folder": "static",
        "file_path": None,
        "file": None,
        "MAIL_SERVER": None,
        "MAIL_PORT": None,
        "MAIL_USE_TLS": False,
        "MAIL_USE_SSL": False,
        "MAIL_DEBUG": False,
        "MAIL_USERNAME": None,
        "MAIL_PASSWORD": None,
        "MAIL_DEFAULT_SENDER": None,
        "MAIL_MAX_EMAILS": None,
        "MAIL_SUPPRESS_SEND": None,
        "MAIL_ASCII_ATTACHMENTS": False
    }
    __setters = [
        "username",
        "password",
        "dsn",
        "database",
        "connection",
        "secret_key",
        "port",
        "location",
        "upload_folder",
        "file",
        "file_path",
        "MAIL_SERVER",
        "MAIL_PORT",
        "MAIL_USE_TLS",
        "MAIL_USE_SSL",
        "MAIL_DEBUG",
        "MAIL_USERNAME",
        "MAIL_PASSWORD",
        "MAIL_DEFAULT_SENDER",
        "MAIL_MAX_EMAILS",
        "MAIL_SUPPRESS_SEND",
        "MAIL_ASCII_ATTACHMENTS"
    ]

    def __init__(self):
        pass

    @staticmethod
    def config(name):
        return Stagging.__conf[name]

    @staticmethod
    def set(name, value):
        if name in Stagging.__setters:
            Stagging.__conf[name] = value
        else:
            raise NameError("Name not accepted in set() method")


class ProductionConfig():
    DEBUG = False
    TESTING = False
    DEVELOPMENT = False
    JWT_SECRET_KEY = GetConfig.config('secret_key')
    SQLALCHEMY_DATABASE_URI = '{connection}://{user}:{pw}@{host}:{port}/{db}'.format(
        connection='{cfg}'.format(cfg=GetConfig.config('connection')),
        user='{username}'.format(username=GetConfig.config('username')),
        pw='{password}'.format(password=GetConfig.config('password')),
        host='{host}'.format(host=GetConfig.config('dsn')),
        port='{port}'.format(port=GetConfig.config('port')),
        db='{database}'.format(database=GetConfig.config('database'))
    )

class StaggingConfig():
    DEBUG = True
    TESTING = True
    DEVELOPMENT = True
    JWT_SECRET_KEY =  Stagging.config('secret_key')
    SQLALCHEMY_DATABASE_URI = '{connection}://{user}:{pw}@{host}:{port}/{db}'.format(
        connection='{cfg}'.format(cfg=Stagging.config('connection')),
        user='{username}'.format(username=Stagging.config('username')),
        pw='{password}'.format(password=Stagging.config('password')),
        host='{host}'.format(host=Stagging.config('dsn')),
        port='{port}'.format(port=Stagging.config('port')),
        db='{database}'.format(database=Stagging.config('database'))
    )


class DevelopmentConfig():
    DEVELOPMENT = True
    DEBUG = True
    JWT_SECRET_KEY = Development.config('secret_key')
    SQLALCHEMY_DATABASE_URI = '{connection}://{user}:{pw}@{host}:{port}/{db}'.format(
        connection='{cfg}'.format(cfg=Development.config('connection')),
        user='{username}'.format(username=Development.config('username')),
        pw='{password}'.format(password=Development.config('password')),
        host='{host}'.format(host=Development.config('dsn')),
        port='{port}'.format(port=Development.config('port')),
        db='{database}'.format(database=Development.config('database'))
    )

app_config = {
    'development': DevelopmentConfig,
    'stagging': StaggingConfig,
    'production': ProductionConfig
}