from flask import Flask,session, render_template, request, url_for, json, Response
from flask_cors import CORS
from config import app_config
import os
from datetime import timedelta
from flask_qrcode import QRcode

from services.distances import services_distance as services

def create_app(env_name):
    app = Flask(__name__, static_url_path="/static", static_folder='static', template_folder='templates')
    app.config.from_object(app_config[env_name])
    app.register_blueprint(services, url_prefix='/distance')

    @app.route('/', methods=['GET'])
    def index():
        return custom_response({'data':None, 'message':'first run app', 'status':200 },200)

    def custom_response(res, status_code):
        return Response(
            mimetype="application/json",
            response=json.dumps(res),
            status=status_code)

    return app